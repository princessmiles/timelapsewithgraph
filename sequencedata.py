# external dependencies: matplotlib, pillow

import matplotlib.pyplot as plt
from PIL import ImageOps, Image
from imagecapture import dest_folder
import os, io

# Get your data here. For below, it's structured so that
# data is a list of two lists, the first with x values and
# second with y values.
data = None


# creates subfolder for pictures with data on it
new_folder = os.path.join(dest_folder, "with_graph")
if not os.path.exists(new_folder):
    os.mkdir(new_folder)

for num, img_name in enumerate(sorted(os.listdir(dest_folder))):
    full_img_name = os.path.join(dest_folder, img_name)
    new_img_name = os.path.join(new_folder, img_name)

    plt.plot(data[0][:num], data[1][:num], label="Data 1")
    plt.figure(figsize=(2,2), dpi=70) # may have to play around with this
    plt.axes([0, 0, 0, 0]) # in format [xmin, xmax, ymin, ymax]
    plt.xlabel("X axis label")
    plt.ylabel("Y axis label")
    plt.legend()

    buf = io.BytesIO()
    plt.savefig(buf, format="png")
    buf.seek(0)

    graph = ImageOps.expand(Image.open(buf))
    img = Image.open(full_img_name)
    img.paste(graph)
    img.save(new_img_name)

