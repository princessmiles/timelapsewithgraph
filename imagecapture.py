# external dependencies: apscheduler & opencv-python
# Webcam required, of course
# Use ctrl-C to exit script

from apscheduler.schedulers.blocking import BlockingScheduler
import cv2, os, math
from datetime import datetime

# since this script needs to run continually, it could be made
# sure that if it ever fails and exits that it should restart
# by using a cron file (see here: https://help.ubuntu.com/community/CronHowto)

################## User customizations ##################
dest_folder = "[your full path to destination folder here]"
freq_per_hr = 4 # number of pictures an hour, evenly spaced
# must divide an hour evenly into whole number minutes
num_zeros = 4 # how many decimals for file names
extension = ".png"
init_time = {
    "year": 2021,
    "month": 10,
    "day": 25,
    "hour": 12,
    "minute": 45
} # time of frame you want to be 0 for file name
#########################################################

cam = cv2.VideoCapture(0)
sched = BlockingScheduler()
minute = ",".join([str(int(60/freq_per_hr*i)) for i in range(freq_per_hr)])
count = math.floor((datetime.now() - datetime(**init_time)).total_seconds() * freq_per_hr / 60**2)

@sched.scheduled_job("cron", minute=minute)
def takePicture():
    global count
    _, frame = cam.read()
    cv2.imwrite(os.path.join(dest_folder, str(count).zfill(num_zeros) + extension), frame)
    count += 1

sched.start()