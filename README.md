## Dependencies

You can get all of these using `pip` in the command line:

- `opencv-python` (imported as `cv2`)
- `pillow` (imported as `PIL`)
- `matplotlib`
- `apscheduler`

## Steps

`imagecapture.py` will handle the pictures. Might be best to use something you won't need lots of access to, like a cheap Raspberry Pi or something. Resolution on the webcam might need to be tinkered with.

## Note

The code I wrote when I first did this project got deleted, so this is what I can remember but I haven't tested it. You'll probably run into bugs (sorry) but this is the bare bones of it.